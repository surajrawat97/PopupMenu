package com.example.ssr.popupmenu;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
Button b;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b= (Button) findViewById(R.id.button);
       b.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                PopupMenu p=new PopupMenu(MainActivity.this,b);
                p.getMenuInflater().inflate(R.menu.popup,p.getMenu());
                p.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item)
                    {
                        int i=item.getItemId();
                        if(i==R.id.item1)
                        {
                        b.setBackgroundColor(Color.RED);

                    }
                    else if(i==R.id.item2){b.setBackgroundColor(Color.BLUE);}

                        else if(i==R.id.item3){b.setBackgroundColor(Color.YELLOW);}
                        return false;
                    }
                });

                p.show();
            }
        });
    }
}
